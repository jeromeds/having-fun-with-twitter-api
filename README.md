#A Little Twitter Page

##Roadmap
###Part 1: get it.
* ~~Put in place a REST API in php in order to work with Twitter API~~
* ~~Use the Twitter API to load the 30 most recent tweets from 3 users~~
* Each tweet should include at minimum:
 * ~~The tweet content~~
 * ~~A well-formatted created_at date~~
 * ~~A link to the tweet.~~
 * For retweets and mentions, the username should be included.
* ~~Bonuses: Make the columns flexible for screen width so the columns fill 100% of the page width. Layout could be as small as 320px wide. Column arrangment is open-ended.~~

###Part 2: Meta
* ~~Make an "edit layout" view that has a form to change the layout settings.~~
* ~~Use HTML5 LocalStorage to store and load the layout settings.~~
* Configurable settings could possibly include:
 * ~~The order of the columns.~~
 * The time range of the tweets shown.
 * ~~The number of tweets shown in each column.~~
 * ~~The overall palette/skin of the page.~~
* ~~The "edit layout" panel can appear either on the same page as the tweets page, on its own page, or embedded within the tweets layout~~
* ~~Straightforward way to toggle between edit and view modes, and it should be clear to the user which mode they are currently in.~~
* ~~BONUSES: Use an interaction (like drag and drop) instead of a form field to order the columns.~~