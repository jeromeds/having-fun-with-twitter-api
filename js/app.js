'use strict';

var myApp = angular.module('myApp', [
  'ngRoute',
  'angular-underscore',
  'LocalStorageModule',
  'ui.sortable'
]);

myApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: 'view.html',
		controller: 'homeCtrl'
	})
	.otherwise({redirectTo: '/'});
}]);

myApp.service('API', ['$http', function($http){
	return {
		get: function(settings, screenName) {
			return $http.get('api/index.php?url=' + encodeURIComponent('statuses/user_timeline.json?screen_name=' + screenName + '&count=' + settings.count));
		}
	};
}]);

myApp.service('getFeedsService', ['$http', '$q', 'API', function($http, $q, API){
	return {
		get: function(settings, screenName) {
      var deferred = $q.defer();
      API.get(settings, screenName).success(function(res){
        deferred.resolve(res);
      });
     return deferred.promise;
    }
	};
}]);

myApp.filter("jsDate", function () {
	return function (x) {
		return Date.parse(x);
	};
});

myApp.controller('homeCtrl', ['$scope', 'API', 'localStorageService', 'getFeedsService', '$rootScope',
  function($scope, API, localStorageService, getFeedsService, $rootScope) {
    // Set variables
    $scope.feeds = [];

    // Set localStorage
    if(_.isNull(localStorageService.get('settings'))){
      $scope.settings = {
        count: 30,
        theme: 'bootstrap',
        feeds:'LaughingSquid,AppDirect,TechCrunch'
      };
      localStorageService.set('settings', $scope.settings);
    }else{
      $scope.settings = localStorageService.get('settings');
    }

    var orderFeeds = function(){
      var feedsOrder = $scope.settings.feeds.split(',');
      for(var i = 0; i < feedsOrder.length; i += 1){
        $scope.feeds.push({name:feedsOrder[i]});
      }
    };

    var getFeeds = function(){
      var i = 1;
      _.each($scope.feeds, function(feed){
        getFeedsService.get($scope.settings, feed.name).then(function(res){
          _.extend(_.findWhere($scope.feeds, {name: res[0].user.screen_name}), {data: res});
        });
      })
    };

    var getFeedsOrder = function(){
      return $scope.feeds.map(function(i){
        return i.name;
      }).join(',');
    };

    $scope.setTheme = function(){
      $rootScope.theme = $scope.settings.theme;
    };

    $scope.edit = function(){
      if($scope.editmode){
        // Edit mode
        $('body').addClass('edit');
        $scope.sortableOptions.disabled = false;
      }else{
        // Save mode
        $('body').removeClass('edit');
        localStorageService.set('settings', $scope.settings);
        getFeeds();
        $scope.sortableOptions.disabled = true;
      }
    };

    $scope.sortableOptions = {
      'disabled': true,
      stop: function(e, ui) {
        $scope.settings.feeds = getFeedsOrder();
        localStorageService.set('settings', $scope.settings);
      }
    };

    //Init
    orderFeeds();
    getFeeds();
    $scope.setTheme();
  }
]);