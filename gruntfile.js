/*global module:false*/
module.exports = function(grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Configurable paths for the application
  /*var appConfig = {
    root: require('./bower.json').appPath,
    dist: 'dist',
    src: 'src',
    conf : 'conf'
  };*/

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
        ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      css: {
        files: ['less/*.less'],
        tasks: ['less'],
        options: {
          spawn: false
        }
      }

    },
   connect: {
        custom_base: {
            options: {
                base: 'test'
            }
        },
        custom_port: {
            options: {
                port: 9000,
                keepalive:true
            }
        }
    },
    less: {
        production: {
            options: {
                paths: ["css"],
                yuicompress: true
            },
            files: {
                "css/styles.min.css": "less/styles.less"
            }
        }
    },
    /*,
    uglify: {
        cms: {
            files: {
                'cms/assets/js/libs.min.js': [
                    'bower_components/jquery/jquery.min.js',
                    'bower_components/jquery-ui/ui/jquery-ui.js',
                    'bower_components/underscore/underscore-min.js',
                    'bower_components/bootstrap/js/modal.js'
                ]
            }
        }
    }*/
    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['index.html']
      }
    }
  });

  // By default, lint and run all tests.
  grunt.registerTask('default', ['wiredep', 'less', 'watch']);
  //grunt.registerTask('build', ['less', 'uglify']);
};